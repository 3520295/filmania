(ns filmania.core
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io])
  (:use midje.sweet))


(defn csv-seq
  "Retourne une séquence à partir d'un fichier CSV."
  [filename]
  (with-open [in-file (io/reader filename)]
    (doall
     (csv/read-csv in-file))))

(defn parse-movie
  "Construit un enregistrement de film depuis un entrée lue depuis CSV."
  [title-year genres]
  (let [r (re-seq #"(.+) \((\d\d\d\d)\)$" title-year)
        title (get (first r) 1)]
    (try
      (let [year (Integer/parseInt (get (first r) 2))]
        {:title title
         :year year
         :genres (set (filter #(not= % "(no genres listed)") (clojure.string/split genres #"\|")))})
      (catch Exception _ nil))))

(defn movie-map
  "Construit une map de films à partir d'un base en CSV."
  [csv]
  (reduce (fn [m [movie-id title-year genres]]
            (if-let [movie (parse-movie title-year genres)]
              (assoc m (Integer/parseInt movie-id) movie)
              m))
          {} csv))

;; Attention: gros fichier
(def movie-filename "resources/ml-latest-small/movies.csv")

(def movies (movie-map (rest (csv-seq movie-filename))))


;;; === Question 0 ===

;; (clojure.pprint/pprint (take 10 movies))


;;; === Question 1 ===

;;; Q: Combien y a-t-il de films dans la base ?   (rappel : répondre avec un fact Midje)


;;; Q: Combien y a-t-il de films de science-fiction ?


;;; Q: Combien y a-t-il de films de romance ?


;;; === Question 2 ===

;;; all-genres


;;; films-by-genre



;;; === Question 3 ===

;;; card-genre

;;; Q: quel est le genre le plus représenté dans la base ?

;;; Q: quel est le genre le moins représenté dans la base ?


;;; === Question 4 ===

;;; card-genre

;;; Q: quel est le genre le plus représenté dans la base ?

;;; Q: quel est le genre le moins représenté dans la base ?

;;; === Question 5 ===

;;; movie-avg-rating

;;; Q: quels sont les films les mieux notés, en moyenne, dans la base ?

;;; Q: quels sont les films les moins bien notés ?

;;; Q: quellle est la note moyenne de la base de films ?

;;; === Question 6 ===

;;; Q: quels sont les utilisateurs les plus "sympathiques" ?

;;; Q: quels sont les utilisateurs les plus "critiques" ?

;;; Q: quel est le film de science-fiction le mieux noté ?

;;; Q: quel est le film de science-fiction le moins bien noté ?



;;; ----
;;; Bon film !

